# uKey.email UI

## Prerequisites
- Node: [https://nodejs.org/en/download/]
- Git:[https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- Source code: [https://git-scm.com/book/en/v2/Getting-Started-Installing-Git]

## Setup Project Code
- Clone the repository
    - ```git clone https://git-scm.com/book/en/v2/Getting-Started-Installing-Git```
- Go to the frontend directory
    - ```cd frontend```
- Install all dependancies
    - ```npm install```
    - ```npm start```

## Open [http://localhost:3000] to view it in the browser.
###For Creating Static code analysis report, you can use the below commnad:-
`node_modules/.bin/eslint --max-warnings 2 --ext .js --f checkstyle . > eslint.xml`