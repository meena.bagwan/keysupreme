import { createGlobalStyle } from "styled-components";
import { device } from "./device";

export const GlobalStyle = createGlobalStyle`
html, body {
  background-color: ${props => props.theme.backgroundColor};
  color: ${props => props.theme.color};
  height: 100%;
  overflow: hidden;
}

#root {
  height: 100%;
}

.hide-scrollbar {
 scrollbar-width: none; /* Firefox */
  -ms-overflow-style: none;  /* IE 10+ */
  &::-webkit-scrollbar {
    width: 0px;
    background: transparent; /* Chrome/Safari/Webkit */
  }
}

.icon {
  width: 24px;
  height: 24px;

  @media ${device.mobileS} {
    width: 16px;
    height: 16px;
  }

  @media ${device.tablet} {
   width: 20px;
    height: 20px;
  }
  @media ${device.laptop} {
    width: 24px;
    height: 24px;
  }
}

.overlay {
  display: block; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

.loader {
  position: fixed;
  height: 2em;
  width: 2em;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}

.cal-height {
  height: calc(100vh - 120px);
  overflow: hidden;
}

.full-height {
  height: calc(100vh - 100px);

  @media ${device.mobileS} {
   height: calc(100vh - 400px);
  }

  @media ${device.tablet} {
   height: calc(100vh - 100px);
  }
}

`;
