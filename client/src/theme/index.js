import { Light } from './LightTheme';
import { Dark } from './DarkTheme';

const theme = {
  light: Light,
  dark: Dark,
};

export default theme;
