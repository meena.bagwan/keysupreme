import React from "react";
import "./App.css";
import MainRouter from "./router/MainRouter";
import { ThemeProvider } from "styled-components";
import theme from "./theme";
import store from "./store";
import { Provider } from "react-redux";
import { GlobalStyle } from "./theme/GlobalStyle";

const App = () => {
  return (
    <Provider store={ store }>
      <ThemeProvider theme={ theme[localStorage.getItem("theme") || 'light'] }>
        <>
          <GlobalStyle />
          <MainRouter />
        </>
      </ThemeProvider>
    </Provider>
  );
};

export default App;
