import * as yup from "yup";
// import { required, email, string } from "yup";

export const checkComposeMailSchema = yup.object().shape({
  fromAddress: yup
    .string()
    .email("From address should be valid email address")
    .required("From address is required"),
  fromName: yup.string().required("From name is required"),
  html: yup.mixed().required(),
  subject: yup.string().required("Subject is required"),
  text: yup.string().required("Content is required"),
  to: yup
    .string()
    .when(["cc", "bcc"], {
      is: (cc, bcc) => cc === "" && bcc === "",
      then: yup
        .string()
        .email("To address should be valid email address")
        .required("Please specify at least one recipient."),
      otherwise: yup.string().notRequired()
    }),
  cc: yup.string().email("CC address should be valid email address"),
  bcc: yup.string().email("BCC address should be valid email address")
});
