import styled, { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`

.detail {
  margin: 0 !important;
  padding:  0;

  .exit-fullscreen {
    padding: 15px !important;
  }
  .rdw-editor-main {
    height: 200px !important;
   white-space: pre-line;
  }
}
`;

export const FloatedDiv = styled.div`
  position: fixed;
  right: 9%;
  bottom: 0;
  box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  width: 510px;
`;

export const FeaturedHeader = styled.div`
  background: #262626;
  color: white;
  padding: 10px;
  cursor: pointer;
`;

export const NewEmailContainer = styled.div`
  background: #f6f6f6;
  color: #212121;
  font-style: normal;
  font-weight: bold;
  line-height: 24px;

  .editor {
    font-weight: 400;
  }
`;

export const To = styled.span`
  font-size: 16px;
`;

export const SubjectOfEmail = styled.span`
  font-size: 22px;
`;
