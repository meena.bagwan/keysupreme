import React, { Component } from "react";
import { GlobalStyle, FeaturedHeader, FloatedDiv } from "./style";

import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { Container, Row, Col } from "react-bootstrap";
import { connect } from "react-redux";
import { hideSidebar } from "../../store/actions/headerActions";
import { setFullScreenMode,setMinimiseScreenMode } from "../../store/actions/mailAction";
import { Link } from "react-router-dom";

import NewEmailPage from "../email/NewEmailPage";
class ExitFullScreen extends Component {
  state = {
    minimize: false
  };

  minimizeScreen = () => {
    this.props.setMinimiseScreenMode(false);
    this.setState(preState => ({ minimize: !preState.minimize }));
  };

  closeScreen = () => {
    this.props.setMinimiseScreenMode(false);
    this.props.setFullScreenMode(true);
  };

  render() {
    const { minimize } = this.state;

    return (
      <>
        <GlobalStyle />
        <FloatedDiv>
          <FeaturedHeader>
            <Container>
              <Row>
                <Col md={8}>New email</Col>
                <Col md={4} className="text-right">
                  <img
                    src="assets/images/minimize.png"
                    alt="Minimize Screen"
                    className="pl-3"
                    onClick={this.minimizeScreen}
                  />
                  <Link to="/newEmailPage">
                    <img
                      src="assets/images/fullscreen.png"
                      alt="Full Screen"
                      className="pl-3"
                    />
                  </Link>

                  <img
                    src="assets/images/close.png"
                    alt="Close Screen"
                    className="pl-3"
                    onClick={this.closeScreen}
                  />
                </Col>
              </Row>
            </Container>
          </FeaturedHeader>
          {!minimize && <NewEmailPage {...this.props} />}
        </FloatedDiv>
      </>
    );
  }
}

const mapState = state => {
  const { openSidebar } = state.headerReducer;
  return {
    openSidebar
  };
};

const mapDispatch = dispatch => {
  return {
    hideSidebar: () => {
      dispatch(hideSidebar());
    },
    setFullScreenMode: data => {
      dispatch(setFullScreenMode(data));
    },
    setMinimiseScreenMode: data=> {
      dispatch(setMinimiseScreenMode(data));
    }
  };
};

export default connect(
  mapState,
  mapDispatch
)(ExitFullScreen);
