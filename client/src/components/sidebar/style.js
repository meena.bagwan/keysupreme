import styled, { createGlobalStyle } from 'styled-components';
import { device } from '../../theme/device';

export const GlobalStyle = createGlobalStyle`

.modal.left .modal-dialog{
  position: fixed;
  margin: auto;
  width: 250px;
  height: 100%;
  -webkit-transform: translate3d(0%, 0, 0);
      -ms-transform: translate3d(0%, 0, 0);
      -o-transform: translate3d(0%, 0, 0);
          transform: translate3d(0%, 0, 0);
}

.modal.left .modal-content{
  height: 100%;
  overflow-y: auto;
  border: none;
  border-radius:0;
}

.modal-header .close {
  padding-top: 30px;

  &:focus {
    outline: none;
  }

  @media ${device.mobileS} {
    padding-top: 40px;
  }
  @media ${device.tablet} {
    padding-top: 30px;
  }
}

.modal.left .modal-body{
  padding: 0 30px;
}

/*Left*/
.modal.left.fade .modal-dialog{
  left: -320px;
  -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
    -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
      -o-transition: opacity 0.3s linear, left 0.3s ease-out;
          transition: opacity 0.3s linear, left 0.3s ease-out;
}

.modal.left.fade.show .modal-dialog{
  left: 0;
}

.modal-content {
  background-color: #262626;
  color: #FFF;
}

.close, .close:hover {
  text-shadow: none;
  color: #FFF;
}
.modal-header {
  border-bottom: none;
  padding: 0 1rem;
}

.search-icon {
  width: 24px;
  height: 24px;

  @media ${device.mobileS} {
    width: 18px;
    height: 18px;
  }
  @media ${device.tablet} {
    width: 24px;
    height: 24px;
  }

}
`;

export const ImgCover = styled.div`
  width: 150px;
  height: 150px;
  margin: 0 auto;
  margin-bottom: 10px;

  .testimonial__image {
    width: 100%;
    height: 100%;
    border-radius: 50%;
    border: 3px solid #fff;
  }
`;
export const UserName = styled.div`
  font-size: 16px;
`;

export const HelloText = styled.div`
  font-size: 24px;
  letter-spacing: 0.8px;
  margin-bottom: 35px;
  width: 186.42px;
  height: 57px;
  font-family: sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  line-height: 34px;
  color: #ffffff;
`;

export const Menu = styled.ul`
  list-style: none;
  padding: 0;
`;

export const MenuItem = styled.li`
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 24px;
  letter-spacing: 0.8px;
  cursor: pointer;
  &:not(last-child) {
    padding: 20px 0;

    @media ${device.mobileS} {
      padding: 10px 0;
    }
    @media ${device.tablet} {
      padding: 20px 0;
    }
  }

  &:before {
    content: "";
    display: inline-block;
    height: 24px;
    width: 24px;
    background: url(${(props) => props.iconImage}) no-repeat top center;
    margin-right: 20px;
    background-size: contain;
    vertical-align: middle;
  }
`;
