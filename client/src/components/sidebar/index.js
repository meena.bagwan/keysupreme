import React, { Component } from "react";
import {
  GlobalStyle,
  ImgCover,
  UserName,
  HelloText,
  Menu,
  MenuItem
} from "./style";
import { Modal, Container } from "react-bootstrap";
import { connect } from "react-redux";
import { hideSidebar } from "../../store/actions/headerActions";
import { sidebarLabels } from "../../labelsConstant/enLanguageLabels"; 

class Sidebar extends Component {
  
  constructor(props)
  {
    super(props)
  }
  sentEmailList = () => 
  {
    this.props.history.push("/sentEmailList");
    this.props.hideSidebar();
  }
  deleteEmailList = () => 
  {
    this.props.history.push("/deleteEmailList");
    this.props.hideSidebar();
  }
  draftEmailList = () => {
    this.props.history.push("/draftEmailList");
    this.props.hideSidebar();
  }
  render() {
    return (
      <>
        <GlobalStyle />

        <Modal
          className="left"
          show={this.props.openSidebar}
          onHide={this.props.hideSidebar}
        >
          <div className="modal-header">
            <button
              type="button"
              className="close"
              onClick={this.props.hideSidebar}
            >
              <span aria-hidden="true">
                <img src="assets/images/leftArrow.png" alt="Left Arrow" />
              </span>
              <span className="sr-only">
                <img src="assets/images/left_arrow.png" alt="Left Arrow" />
              </span>
            </button>
          </div>

          <Modal.Body>
            <Container>
              <ImgCover>
                <img
                  src="assets/images/profilePic.jpg"
                  className="img-responsive testimonial__image hidden-sm hidden-xs"
                  alt=""
                />
              </ImgCover>
              <HelloText>
                Hello,
                <UserName>Meena Bagwan</UserName>
              </HelloText>
              <Menu>
                <MenuItem iconImage="assets/images/sent_icon.png" onClick={this.sentEmailList}>
                  {sidebarLabels.SENT}
                </MenuItem>
                <MenuItem iconImage="assets/images/draft_icon.png" onClick={this.draftEmailList}>
                  {sidebarLabels.DRAFT}
                </MenuItem>
                <MenuItem iconImage="assets/images/files_icon.png">
                  {sidebarLabels.FILES}
                </MenuItem>
                <MenuItem iconImage="assets/images/clock_icon.png">
                  {sidebarLabels.SCHEDULED}
                </MenuItem>
                <MenuItem iconImage="assets/images/setting_icon.png">
                  {sidebarLabels.SETTING}
                </MenuItem>
                <MenuItem iconImage="assets/images/trash_icon.png" onClick={this.deleteEmailList}>
                  {sidebarLabels.TRASH}
                </MenuItem>
              </Menu>
            </Container>
          </Modal.Body>
        </Modal>
      </>
    );
  }
}

const mapState = state => {
  const { openSidebar } = state.headerReducer;
  return {
    openSidebar
  };
};

const mapDispatch = dispatch => {
  return {
    hideSidebar: () => {
      dispatch(hideSidebar());
    }
  };
};

export default connect(
  mapState,
  mapDispatch
)(Sidebar);
