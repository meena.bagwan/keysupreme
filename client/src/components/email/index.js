import React, { PureComponent } from "react";
import { Table, Container } from "react-bootstrap";
import * as api from "../../api/apiList";
import {
  loginData,
  setLoader,
  resetLoader,
  setMailSentFlag
} from "../../store/actions/mainActions";

import {
  setMailList,
  getMailList,
  sendMail,
  setComposeMailData
} from "../../store/actions/mailAction";
import {
  ImgCover,
  Name,
  GlobalStyle,
  RowTag,
  FeatureButton,
  RowLine
} from "./style";
import { connect } from "react-redux";
import StickyFeatures from "../widget/StickyFeatures";
import API from "../../api";
import GlobalLoder from "../widget/GlobalLoder";
import ExitFullScreen from "../fullscreen/ExitFullScreen";
import DismissibleAlert from "../widget/DismissibleAlert";
import _ from "lodash";
import moment from "moment";
import { constants } from "../../constant/constant";
class EmailListing extends PureComponent {
  state = {
    featureButton: {},
    toEmailDetails: false,
    isDeleted: false,
    selectedMails: []
  };

  componentDidMount() {
    if (Object.keys(this.props.composeMailData).length > 0) {
      this.props.sendMail(this.props.composeMailData);
    } else {
      this.props.getMailList();
      this.props.sentMailFlag(false);
    }
  }
  handleSubmit = () => {
    this.setState({
      toEmailDetails: true
    });
  };

  toggleMailSelection = (e, list) => {
    e.stopPropagation();

    let selectedMails = [...this.state.selectedMails];

    if (e.shiftKey) {
      selectedMails = selectedMails.includes(list.uid)
        ? selectedMails.filter(mail => mail !== list.uid)
        : [...this.state.selectedMails, list.uid];
    } else {
      selectedMails = selectedMails.includes(list.uid) ? [] : [list.uid];
    }

    this.setState({ selectedMails });
  };

  onMouseEnter = rowId => {
    this.setState({
      featureButton: { [rowId]: true }
    });
  };

  onFlagClick = (e, email, index) => {
    e.stopPropagation();

    API.post(api.SET_FLAG, {
      flags: {
        key: "Flagged",
        value: !email.markAsImp
      },
      uid: email.uid
    }).then(res => {
      let emails = [...this.props.mailList];
      emails = emails.map(emailData =>
        emailData.uid === email.uid
          ? {
              ...emailData,
              markAsImp: !emailData.markAsImp
            }
          : emailData
      );

      this.props.setMailList(emails);
    });
  };

  onDeleteClick = (e, email, index) => {
    this.setState({ isDeleted: false });
    e.stopPropagation();
    API.post(api.SET_FLAG, {
      flags: {
        key: "Deleted",
        value: true
      },
      uid: email.uid
    }).then(res => {
      let mailList = [...this.props.mailList];
      mailList = mailList.filter((mail, i) => mail.uid !== email.uid);
      this.props.setMailList(mailList);
      this.setState({ isDeleted: true });
    });
  };

  onMouseLeave = rowId => {
    this.setState({
      featureButton: { [rowId]: false }
    });
  };

  colorCodeAsLabel = flags => {
    if (flags.includes("update")) {
      return (this.borderColor = "#26A69A");
    } else if (flags.includes("personal")) {
      return (this.borderColor = "#1565C0");
    } else if (flags.includes("work")) {
      return (this.borderColor = "#673AB7");
    } else {
      return (this.borderColor = "#f6f6f6");
    }
  };

  redirectToEmailDetail = (list, index) => {
    let mailList = this.props.mailList;
    mailList[index].flags.push("\\Seen");
    this.props.setMailList(mailList);
    API.post(api.SET_FLAG, {
      flags: {
        key: "Seen",
        value: true
      },
      uid: list.uid
    }).then(res => {});

    this.props.history.push("/email-details", {
      email: list
    });
  };

  onDragStart = (e, mailId) => {
    const { selectedMails } = this.state;
    if (selectedMails.includes(mailId)) {
      e.dataTransfer.setData("id", mailId);
    } else {
      e.preventDefault();
      return false;
    }

    //  e.target.style.opacity = "0.4";
  };

  classifyMailList = mails => {
    let mailData = mails.sort((a, b) => {
      return new Date(b.date) - new Date(a.date);
    });
    let dateWiseData = {};
    let todayMail = [];
    let yesterdayMail = [];
    let weekMail = [];
    let monthMail = [];

    let todayDate = moment().format(constants.DATEFORMAT);
    let yesterdayDate = moment()
      .subtract(1, "days")
      .format(constants.DATEFORMAT);
    let week_from_date = moment()
      .startOf("week")
      .format(constants.DATEFORMAT);
    let week_to_date = moment()
      .endOf("week")
      .format(constants.DATEFORMAT);

    mailData.forEach(element => {
      let mailDate = moment(element.date).format(constants.DATEFORMAT);

      if (
        moment(todayDate, constants.DATEFORMAT).isSame(
          moment(mailDate, constants.DATEFORMAT),
          "day"
        )
      ) {
        todayMail.push(element);
      } else if (
        moment(mailDate, constants.DATEFORMAT).isSame(
          moment(yesterdayDate, constants.DATEFORMAT)
        )
      ) {
        yesterdayMail.push(element);
      } else if (
        moment(mailDate, constants.DATEFORMAT).isBetween(
          moment(week_from_date, constants.DATEFORMAT),
          moment(week_to_date, constants.DATEFORMAT)
        )
      ) {
        weekMail.push(element);
      } else {
        monthMail.push(element);
      }
    });
    dateWiseData["Today"] = todayMail;
    dateWiseData["Yesterday"] = yesterdayMail;
    dateWiseData["This Week"] = weekMail;
    dateWiseData["This Month"] = monthMail;
    return dateWiseData;
  };

  renderMailList = () => {
    const { label } = this.props.currentMailType;
    const { searchText } = this.props;
    const { selectedMails } = this.state;

    this.props.mailList.sort((a, b) => {
      return b.uid - a.uid;
    });
    let filterdEmails = this.props.mailList.filter(
      list =>
        (this.props.isImportant &&
          list.markAsImp === true &&
          (label === "all" || label === "" || list.flags.includes(label))) ||
        (!this.props.isImportant &&
          (label === "all" || label === "" || list.flags.includes(label)))
    );

    //let classifiedMailList = this.classifyMailList(this.props.mailList);
    let classifiedMailList = this.classifyMailList(filterdEmails);

    return Object.keys(classifiedMailList).map(dateKey => {
      return (
        <>
          {Object.keys(classifiedMailList[dateKey]).length !== 0 && (
            <RowTag className="timeline">
              <td width="25%">
                <RowLine>{dateKey}</RowLine>
              </td>
              <td width="70%"></td>
            </RowTag>
          )}
          {classifiedMailList[dateKey].map((list, index) => {
            return (
              <RowTag
                key={list.uid}
                borderColorCode={
                  selectedMails.includes(list.uid)
                    ? "white"
                    : this.colorCodeAsLabel(list.flags)
                }
                onMouseEnter={() => this.onMouseEnter(index)}
                onMouseLeave={() => this.onMouseLeave(index)}
                onDoubleClick={() => this.redirectToEmailDetail(list, index)}
                onClick={e => this.toggleMailSelection(e, list)}
                className={
                  selectedMails.includes(list.uid)
                    ? "white-back draggable"
                    : "grey-back draggable"
                }
                draggable
                onDragStart={e => this.onDragStart(e, list.uid)}
              >
                <td width="25%" className="border-dash">
                  <ImgCover>
                    <img
                      src="/assets/images/profilePic.jpg"
                      className="img-responsive testimonial__image hidden-sm hidden-xs"
                      alt=""
                    />
                  </ImgCover>
                  <Name>
                    {(list.from && list.from[0] && list.from[0].name) || ""}
                  </Name>
                </td>
                <td width="70%" className="position-relative">
                  <p
                    className={
                      this.state.featureButton &&
                      this.state.featureButton[index] &&
                      !this.props.isImportant
                        ? "mail-content width-to-wrap-text"
                        : "mail-content"
                    }
                  >
                    {_.includes(list.flags, "\\Seen") ? (
                      list.subject
                    ) : (
                      <b>{list.subject}</b>
                    )}
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{list.text}
                  </p>
                  {this.state.featureButton &&
                    this.state.featureButton[index] &&
                    (!this.props.isImportant && (
                      <FeatureButton
                        backColor={
                          selectedMails.includes(list.uid) ? "white" : "#f6f6f6"
                        }
                      >
                        <img
                          src="assets/images/delete.png"
                          className="icon"
                          alt="delete"
                          onClick={e => this.onDeleteClick(e, list, index)}
                        />
                        <img
                          src={
                            list.markAsImp
                              ? "assets/images/importantFlag.png"
                              : "assets/images/flagPriority.png"
                          }
                          className="icon"
                          alt="important"
                          onClick={e => this.onFlagClick(e, list, index)}
                        />
                      </FeatureButton>
                    ))}

                  {list.markAsImp && (
                    <FeatureButton
                      backColor={
                        selectedMails.includes(list.uid) ? "white" : "#f6f6f6"
                      }
                    >
                      <img
                        src="assets/images/importantFlag.png"
                        alt="impFlag"
                        className="icon"
                        onClick={e => this.onFlagClick(e, list, index)}
                      />
                    </FeatureButton>
                  )}
                </td>
              </RowTag>
            );
          })}
        </>
      );
    });
  };

  render() {
    const { loading } = this.props;
    const { label } = this.props.currentMailType;

    return (
      // <Container className="mt-5 full-height">
      <Container className="mt-5 full-height">
        <GlobalStyle />
        {this.state.isDeleted && (
          <DismissibleAlert
            message="Email moved to Trash"
            styleClass="alert-info"
          />
        )}
        {this.props.mailSent && (
          <DismissibleAlert
            message="Email Sent Successfully"
            styleClass="alert-info"
          />
        )}
        <Table>
          <tbody>{loading ? <GlobalLoder /> : this.renderMailList()}</tbody>
          {/* <tbody>{this.renderMailList()}</tbody> */}
        </Table>

        {!this.props.fullScreen && <ExitFullScreen {...this.props} />}
        {this.props.fullScreen && <StickyFeatures {...this.props} />}
      </Container>
    );
  }
}

const mapState = state => {
  const { currentMailType, isImportant, searchText } = state.headerReducer;
  const { loading, loginData, mailSent } = state.mainReducer;
  const { mailList, fullScreen, composeMailData } = state.mailReducer;
  return {
    isImportant,
    currentMailType,
    searchText,
    loading,
    loginData,
    mailList,
    mailSent,
    fullScreen,
    composeMailData
  };
};

const mapDispatch = dispatch => {
  return {
    loginData: data => {
      dispatch(loginData(data));
    },
    setLoading: () => {
      dispatch(setLoader());
    },
    resetLoading: () => {
      dispatch(resetLoader());
    },
    setMailList: data => {
      dispatch(setMailList(data));
    },
    sentMailFlag: data => {
      dispatch(setMailSentFlag(data));
    },
    getMailList: data => {
      dispatch(getMailList(data));
    },
    sendMail: data => {
      dispatch(sendMail(data));
    },
    setComposeMailData: data => {
      dispatch(setComposeMailData(data));
    }
  };
};
export default connect(
  mapState,
  mapDispatch
)(EmailListing);
