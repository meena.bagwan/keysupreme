import React, { PureComponent } from "react";
import * as api from "../../api/apiList";
import API from "../../api";
import {
    getDraftList
} from "../../store/actions/mailAction";

import { connect } from "react-redux";
import {
  GlobalStyle,
  RowTag,
  ImgCover,
  Name
} from "./style";
import { Table, Container } from "react-bootstrap";
import GlobalLoder from '../widget/GlobalLoder';
class DraftEmailList extends PureComponent {

  state = {
    DraftMails: []
  };
  constructor(props) {
    super(props)
  }
  componentDidMount() {
    this.props.getDraftList();
  }
  colorCodeAsLabel = flags => {
    if (flags.includes("update")) {
      return (this.borderColor = "#26A69A");
    } else if (flags.includes("personal")) {
      return (this.borderColor = "#1565C0");
    } else if (flags.includes("work")) {
      return (this.borderColor = "#673AB7");
    } else {
      return (this.borderColor = "white");
    }
  };
  redirectToEmailDetail = (list) => {
    this.props.history.push("/email-details", {
      email: list
    });
  };
  renderMailList = () => {
    const  DraftMails  = this.props.draftMailList;
    console.log("@@@@@@@@@@ DeleteMails",DraftMails);
    return DraftMails.map((list) => {
      return (
        <RowTag 
         onClick={() => this.redirectToEmailDetail(list)}
          key={list.uid}
          borderColorCode={this.colorCodeAsLabel(list.flags)}
          
        >
          <td width="25%" className="border-dash">
            <ImgCover>
              <img
                src="/assets/images/profilePic.jpg"
                className="img-responsive testimonial__image hidden-sm hidden-xs"
                alt=""
              />
            </ImgCover>
            <Name>{(list.from && list.from[0] && list.from[0].name) || ""}</Name>
          </td>
          <td width="70%" className="position-relative">
            <p className={"mail-content width-to-wrap-text"}>
              {list.subject}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{list.text}
            </p>
          </td>
        </RowTag>
      )
    })
  }

  render() {
    const { loading } = this.props;
    return (
      <Container className="mt-5 full-height">
        <GlobalStyle />
        <Table>
          <tbody>{loading ? <GlobalLoder /> : this.renderMailList()}</tbody>
        </Table>
      </Container>
    );
  }
}

const mapState = state => {
  const { draftMailList } = state.mailReducer;
  return {
    draftMailList
  };
};

const mapDispatch = dispatch => {
  return {
    getDraftList: () => {
      dispatch(getDraftList());
    }
  };
};
export default connect(
  mapState,
  mapDispatch
)(DraftEmailList);
