import React from "react";
import { FeatureButton } from "./style";

export const MailFeature = () => {
  return (
    <FeatureButton>
      <img src="assets/images/delete.png" className="icon" alt="delete" />
      <img
        src="assets/images/flagPriority.png  "
        className="icon"
        alt="important"
      />
    </FeatureButton>
  );
};
