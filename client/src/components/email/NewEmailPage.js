import React, { Component } from "react";
import { Container, Row, Col, Alert } from "react-bootstrap";
import { GlobalStyle, NewEmailContainer, To, Cc, Bcc } from "./style";
import StickyFeatures from "../widget/StickyFeatures";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { Editor } from "@tinymce/tinymce-react";
import proofReadingOutput from "../../data/proofReadingOutput.json";
import axios from "axios";
import DismissibleAlert from "../widget/DismissibleAlert";
class NewEmailPage extends Component {
  state = {
    content: "",
    htmlContent: "",
    subject: "",
    to: "",
    cc:"",
    bcc:"",
    error: {},
    toClick: false,
    ccClick: false,
    bccClick: false
  };

  setError = error => {
    this.setState({ error: error });
  };

  handleEditorChange = e => {
    this.setState({
      content: e.target.getContent({ format: "text" }),
      htmlContent: e.target.getContent()
    });
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  toClick = () => {
    this.setState({ toClick: true })
  }
  ccClick = () => {
    this.setState({ ccClick: true })
  }
  bccClick = () => {
    this.setState({ bccClick: true })
  }

  render() {
    return (
      <Container className="mt-5 detail">
        <GlobalStyle />
        {this.state.error && this.state.error.hasError && (
          <>
            <div className="alert-info">{this.state.error.message}</div>
          </>
        )}

        <NewEmailContainer className="p-3 p-md-5 exit-fullscreen">
          <Row>
            <Col xs={12} md={12} lg={12}>
              <To>
                To:
                <input
                  className="input-control mt-2 mt-md-0"
                  type="text"
                  name="to"
                  onChange={this.handleChange}
                  onClick={this.toClick}
                />
              </To>
              {this.state.toClick && !this.state.ccClick && (
                <Cc onClick={this.ccClick}>
                  Cc
                </Cc>
              )}
              {this.state.toClick && !this.state.bccClick && (
                <Bcc onClick={this.bccClick}>
                  Bcc
                </Bcc>
              )}
              <img
                src="/assets/images/attach.png"
                alt="attachment"
                className="float-right icon"
              />
            </Col>
          </Row>

          {this.state.ccClick && (
            <Row>
              <Col>
              Cc:
                <input
                  className="field-control"
                  type="text"
                  name="cc"
                  onChange={this.handleChange}
                />
              </Col>
            </Row>
          )}
          {this.state.bccClick && (
            <Row>
              <Col>
              Bcc:
                <input
                  className="field-control"
                  type="text"
                  name="bcc"
                  onChange={this.handleChange}
                />
              </Col>
            </Row>
          )}

          <Row>
            <Col xs={12} md={12} lg={12} className="subject-line pt-4">
              <input
                className="subject-control mt-2 mt-md-0"
                type="text"
                name="subject"
                placeholder="Subject of email..."
                onChange={this.handleChange}
              />
            </Col>
          </Row>

          <Row className="editorHeight">
            <Col xs={12} md={12} lg={12} className="editor">
              <Editor
                initialValue='<p class="placeholder">Start writing...</p>'
                apiKey="naqisewzgrdnhgaicvtqcga5muarp18s785ago6ewgeudcmu"
                init={{
                  body_id: "editor",
                  selector: "textarea",
                  branding: false,
                  statusbar: false,
                  spellchecker_active: true,
                  init_instance_callback: function (editor) {
                    editor.on("SpellcheckEnd", function (e) {
                      editor.execCommand("mceSpellCheck");
                    });
                  },
                  plugins: "spellchecker",
                  toolbar:
                    "fontselect fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright alignjustify | spellchecker",
                  menubar: false,
                  content_style:
                    ".mce-spellchecker-word { background-image: none;border-bottom: 3px solid red!important} .mce-content-body{ margin: 0!important}.placeholder {color: #bebebe!important;}",
                  setup: function (editor) {
                    editor.on("focus", function (e) {
                      if (
                        editor.getContent() ===
                        '<p class="placeholder">Start writing...</p>'
                      ) {
                        editor.setContent("");
                      }
                    });

                    editor.on("keyup", function (e) {
                      if (e.keyCode === 32) {
                        let words = editor.getBody().textContent.split(" ");
                        if (words.length % 5 === 0) {
                          editor.execCommand("mceSpellCheck");
                        }
                      }
                    });
                  },
                  spellchecker_callback: function (method, text, success) {
                    var words = text.match(this.getWordCharPattern());
                    var data = {};
                    if (words) {
                      data["words"] = Object.assign({}, words);
                      data["lang"] = this.getLanguage();
                      var suggestions = {};
                      if (method === "spellcheck") {
                        // axios
                        // .post("http://192.168.2.240:8000/proofreading/v1.0/feature", { data: text })
                        // .then(res => {
                        //   console.log(res.data.errors);
                        //   let errors = res.data.errors;
                        //   for (var i = 0; i < errors.length; i++) {
                        //     console.log("@@@@@@@@@@@@ ", errors[i]["replacements"]);
                        //     suggestions[errors[i]["actualword"]] = errors[i]["replacements"];
                        //   }
                        //   success({ words: suggestions, dictionary: [] });
                        // });
                        let errors = proofReadingOutput.errors;
                        for (var i = 0; i < errors.length; i++) {
                          suggestions[errors[i]["actualword"]] =
                            errors[i]["replacements"];
                        }
                        success({ words: suggestions, dictionary: [] });
                      }
                    } else {
                      success({});
                    }
                  }
                }}
                onChange={this.handleEditorChange}
              />
            </Col>
          </Row>
        </NewEmailContainer>
        <StickyFeatures
          {...this.props}
          data={this.state}
          setError={this.setError}
        />
      </Container>
    );
  }
}

export default NewEmailPage;
