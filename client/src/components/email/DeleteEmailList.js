import React, { PureComponent } from "react";
import * as api from "../../api/apiList";
import API from "../../api";
import {
  setMailList,
  getDeleteList
} from "../../store/actions/mailAction";
import {
  setLoader,
  resetLoader
} from "../../store/actions/mainActions";
import { connect } from "react-redux";
import {
  GlobalStyle,
  RowTag,
  ImgCover,
  Name
} from "./style";
import { Table, Container } from "react-bootstrap";
import GlobalLoder from '../widget/GlobalLoder';
class DeleteEmailList extends PureComponent {

  state = {
    DeleteMails: []
  };
  constructor(props) {
    super(props)
  }
  componentDidMount() {
    this.props.getDeleteList();
  }
  colorCodeAsLabel = flags => {
    if (flags.includes("update")) {
      return (this.borderColor = "#26A69A");
    } else if (flags.includes("personal")) {
      return (this.borderColor = "#1565C0");
    } else if (flags.includes("work")) {
      return (this.borderColor = "#673AB7");
    } else {
      return (this.borderColor = "white");
    }
  };
  redirectToEmailDetail = (list) => {
    this.props.history.push("/email-details", {
      email: list
    });
  };
  renderMailList = () => {
    const  DeleteMails  = this.props.deleteMailList;
    console.log("@@@@@@@@@@ DeleteMails",DeleteMails);
    return DeleteMails.map((list) => {
      return (
        <RowTag 
         onClick={() => this.redirectToEmailDetail(list)}
          key={list.uid}
          borderColorCode={this.colorCodeAsLabel(list.flags)}
          
        >
          <td width="25%" className="border-dash">
            <ImgCover>
              <img
                src="/assets/images/profilePic.jpg"
                className="img-responsive testimonial__image hidden-sm hidden-xs"
                alt=""
              />
            </ImgCover>
            <Name>{(list.from && list.from[0] && list.from[0].name) || ""}</Name>
          </td>
          <td width="70%" className="position-relative">
            <p className={"mail-content width-to-wrap-text"}>
              {list.subject}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{list.text}
            </p>
          </td>
        </RowTag>
      )
    })
  }

  render() {
    const { loading } = this.props;
    return (
      <Container className="mt-5 full-height">
        <GlobalStyle />
        <Table>
          <tbody>{loading ? <GlobalLoder /> : this.renderMailList()}</tbody>
        </Table>
      </Container>
    );
  }
}

const mapState = state => {
  const { currentMailType } = state.headerReducer;
  const { loading } = state.mainReducer;
  const { deleteMailList } = state.mailReducer;

  return {
    currentMailType,
    loading,
    deleteMailList
  };
};

const mapDispatch = dispatch => {
  return {

    setMailList: data => {
      dispatch(setMailList(data));
    },
    setLoading: () => {
      dispatch(setLoader());
    },
    resetLoading: () => {
      dispatch(resetLoader());
    },
    getDeleteList: () => {
      dispatch(getDeleteList());
    }
  };
};
export default connect(
  mapState,
  mapDispatch
)(DeleteEmailList);
