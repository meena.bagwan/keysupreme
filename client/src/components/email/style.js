import styled, { createGlobalStyle } from "styled-components";
import { device } from "../../theme/device";

export const GlobalStyle = createGlobalStyle`

  table {
    table-layout: fixed;
    background: #f6f6f6; 
  }
   .table .timeline {
    background: #e8e8e8;
    border-left: 6px solid #e8e8e8;
    td {
      padding: 60px 0 20px;
    }
    &:first-child{
      display: none;
    }
  }
  .mt-5 {
    margin-top: 6rem!important;
  } 
  .editorHeight{
    height:350px;
  }
  .rdw-editor-toolbar {
    margin-top: 8em;
  }
  .table td {
    .mail-content {
      text-overflow: ellipsis;
      overflow: hidden;
      white-space: nowrap;
      margin: 0;
      padding: 0;
      max-width: 95%;
    }
    vertical-align: middle;
    border-bottom: 1px solid #e8e8e8;
    border-top: 0;
    cursor: pointer;
  }
  
  .width-to-wrap-text {
    max-width: 90% !important;

    @media ${device.mobileS} {
      max-width: 60% !important;
    }

    @media ${device.tablet} {
      max-width: 85% !important;
    }

    @media ${device.laptop} {
      max-width: 90% !important;
    }
  }
  .detail table {
    background-color: white;

    tr:last-child p {
      border-bottom: none;
    }
    td {
      border-bottom: none;
      
    }
  }
  .fieldDisplay {
    display:inline-block
  }
  .wrap-text {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }

  .demo-wrapper {
    display: flex;
    flex-direction: column;
  }
  .rdw-editor-main {
    height: 200px !important;
  }
  .demo-toolbar {
    order:  2
  }
  
  .input-control {
    background-color: transparent;
    border: 0;
    font-family: 'Open Sans', sans-serif;
    font-style: normal;
    font-size: 14px;
    line-height: 24px;
    color: #505050;
    display: inline-block;
    margin-left:14px;
    letter-spacing: 0.8px;
    width: 85%;

    &::placeholder {
      color: #969696
    }

    &:focus {
      outline: unset;
    }

    @media ${device.mobileS} {
      font-size: 12px;
      margin-left:10px;
      width: 85%;
    }

    @media ${device.tablet} {
      font-size: 14px;
      margin-left:10px;
      width: 50%;
    }

     @media ${device.laptop} {
      width: 85%;
    }
  }

  .field-control {
   font-size:16px;
   padding-top:10px;
   background-color: transparent;
   border:0; 
   font-family: 'Open Sans', sans-serif;
   font-style: normal;
   font-weight: bold;
   color: #505050;
   display: inline-block;
   letter-spacing: 0.8px;
   width: 50%;
   
   &::placeholder {
    color: #969696
  }

  &:focus {
    outline: unset;
  }

  @media ${device.mobileS} {
    font-size: 12px;
    margin-left:10px;
    width: 85%;
  }

  @media ${device.tablet} {
    font-size: 14px;
    margin-left:10px;
    width: 50%;
  }

   @media ${device.laptop} {
    width: 85%;
  }
   
  }

  .subject-control {
    background-color: transparent;
    border: 0;
    font-family: 'Open Sans', sans-serif;
    font-style: normal;
    font-size: 22px;
    font-weight: bold;
    color: #505050;
    display: inline-block;
    letter-spacing: 0.8px;
    width: 50%;

     &::placeholder {
      font-family: 'Open Sans', sans-serif;
      color: #212121;
      font-weight: bold;
      opacity: 1 !important;
      font-size: 22px;
      font-style: normal;
    }

    &:focus {
       &::placeholder {
        font-family: 'Open Sans', sans-serif;
        color: #969696;
        font-weight: bold;
        opacity: 1 !important;
        font-size: 22px;
        font-style: italic;
      }
      outline: unset;
    }

    @media ${device.mobileS} {
      font-size: 16px;
      width: 85%;
    }

    @media ${device.tablet} {
      font-size: 22px;
      width: 50%;
    }
  }

  .alert-info {
    display: inline-block;
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 24px;
    color: #FFFFFF;
    background-color:black;
    padding: 8px;
    border-radius: 10px;
    position: fixed;
    top: 23%;
    left: 9%;
  }
  .border-dash {
    position: relative
  &:before {
    content: "";
    background: #e8e8e8
    width: 6px;
    height: 1px;
    position: absolute;
    bottom: -1px;
    left: -6px;
  }
  }


  .tox .tox-toolbar {
    order: 2!important;
    border: none!important;
        // background-color: #f6f6f6!important;
  }
  .tox .tox-edit-area__iframe {
    background-color: #f6f6f6!important;
  }
  .tox-tinymce {
    border: none!important;
    height: 100%!important;
  }
  .tox .tox-edit-area {
    border: none!important;
  }
  .mce-content-body  { 
    margin: 0!important; 
  }
  #editor.placeholder {
    color: #f6f6f6!important; 
  }

  .white-back {
    background: white;
  }
  .grey-back {
    background: #f6f6f6;
  }
`;

export const RowTag = styled.tr`
  border-left: 6px solid ${props => props.borderColorCode};
`;

export const RowLine = styled.span`
  color: #212121;
  width: 95px;
  height: 24px;
  padding-top: 40px;
  padding-bottom: 65px;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 24px;
`;
export const ImgCover = styled.div`
  width: 50px;
  height: 50px;
  display: inline-block;

  .testimonial__image {
    width: 100%;
    height: 100%;
    border-radius: 50%;
    border: 3px solid #fff;
  }
`;

export const Name = styled.p`
  padding-left: 10px;
  display: inline-block;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;

  @media ${device.mobileS} {
    display: none;
  }

  @media ${device.tablet} {
    display: unset;
  }
`;

export const EmailContent = styled.p`
  padding-top: 10px;
  display: inline-block;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
`;

export const RowStyle = styled.p`
  background: #f6f6f6;
`;

export const FeatureButton = styled.div`
  background: ${props => props.backColor}
  position: absolute;
  right: 12px;
  margin-top: -26px;
  img {
    margin: 0 8px;
  }
`;
export const EmailImgCover = styled.span`
  width: 62px;
  height: 60px;
  position: absolute;
  // margin-top: 22px;
  // margin-left: 25px;
  display: inline-block;

  .testimonial__image {
    width: 100%;
    height: 100%;
    border-radius: 50%;
    border: 3px solid #fff;
  }
`;
export const EmailName = styled.h3`
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 24px;
  color: #212121;
  margin: 0;
`;
export const EmailDate = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: #212121;
  display: inline-block;
`;
export const EmailDisplay = styled.div`
  background: #f6f6f6;
  height: 100%;
  margin-top: 6rem !important;
`;

export const UserName = styled.div`
  font-size: 16px;
`;

export const HelloText = styled.div`
  font-size: 24px;
  letter-spacing: 0.8px;
  margin-bottom: 35px;
  width: 186.42px;
  height: 57px;
  font-family: sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  line-height: 34px;
  color: #ffffff;
`;

export const EmailExpand = styled.span`
  float: right;
  cursor: pointer;
  padding: 5px 10px;
`;

export const EmailDetailContent = styled.p`
  width: 92%;
  margin-right: 0;
  margin-left: 3em;
  margin-top: 2em;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  color: #212121;
  border-bottom: 1px solid #e8e8e8;
  padding-bottom: 25px;

  @media ${device.mobileS} {
    margin-left: 0;
    width: unset;
  }

  @media ${device.tablet} {
    margin-left: 3em;
    width: 92%;
  }
`;

export const EmailInfo = styled.div`
  padding-left: 75px;
  margin-top: 6px;
`;

export const NewEmailContainer = styled.div`
  background: #f6f6f6;
  color: #212121;
  font-style: normal;
  font-weight: bold;
  line-height: 24px;

  .editor {
    font-weight: 400;
  }
`;
export const To = styled.span`
  font-size: 16px;

  @media ${device.mobileS} {
    font-size: 12px;
  }
  @media ${device.tablet} {
    font-size: 16px;
  }
`;

export const Cc = styled.span`
  font-size: 16px;
  padding-right:20px;
  cursor:pointer;

  @media ${device.mobileS} {
    font-size: 12px;
  }
  @media ${device.tablet} {
    font-size: 16px;
  }
`;

export const Bcc = styled.span`
  font-size: 16px;
  cursor:pointer;
  @media ${device.mobileS} {
    font-size: 12px;
  }
  @media ${device.tablet} {
    font-size: 16px;
  }
`;

export const Attachment = styled.span`
  padding-left: 200px;
  padding-top: 25px;
`;

export const SubjectOfEmail = styled.span`
  font-size: 22px;

  @media ${device.mobileS} {
    font-size: 16px;
  }
  @media ${device.tablet} {
    font-size: 22px;
  }
`;

export const DeletionNotify = styled.span`
  font-family: Open Sans;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 24px;
  color: #ffffff;
  background-color: black;
  padding: 8px;
  border-radius: 10px;
`;
