import React, { Component } from "react";
import { Container, Table } from "react-bootstrap";
import StickyFeatures from "../widget/StickyFeatures";
import {
  EmailImgCover,
  EmailName,
  EmailDate,
  EmailDetailContent,
  GlobalStyle,
  EmailInfo,
  EmailExpand
} from "./style";

class EmailDetails extends Component {
  constructor(props) {
    super(props);
    this.element = React.createRef();
  }

  componentDidMount() {
    this.element.current.addEventListener("click", this);
  }

  handleEvent(e) {
    let element = e.target;
    let elementToProcess = null;
    if (element.classList.contains("content")) {
      elementToProcess = element;
    } else if (
      element.classList.contains("expand") ||
      element.closest("div").classList.contains("email-info") ||
      element.classList.contains("expand-image")
    ) {
      elementToProcess = element.closest("div").nextSibling;
    }
    elementToProcess && elementToProcess.classList.toggle("wrap-text");
  }

  formatDate(string) {
    var options = { year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(string).toLocaleDateString([], options);
  }
  render() {
    return (
      <Container className="mt-5 detail">
        <GlobalStyle />
        <Table>
          <tbody ref={this.element}>
            <tr>
              <td className="position-relative pt-4 px-4">
                <EmailImgCover>
                  <img
                    src="/assets/images/profilePic.jpg"
                    className="img-responsive testimonial__image hidden-sm hidden-xs"
                    alt=""
                  />
                </EmailImgCover>

                <EmailInfo className="email-info">
                  <EmailName>
                    {this.props.location.state.email.from[0].name}
                    <EmailExpand className="expand">
                      <img
                        src="/assets/images/shape.png"
                        alt="shape"
                        className="expand-image"
                      />
                    </EmailExpand>
                  </EmailName>
                  <EmailDate>{this.formatDate(this.props.location.state.email.date)}</EmailDate>
                </EmailInfo>

                <EmailDetailContent className="content wrap-text">
                  {this.props.location.state.email.text}
                </EmailDetailContent>
              </td>
            </tr>
          </tbody>
        </Table>
        <StickyFeatures {...this.props} />
      </Container>
    );
  }
}

export default EmailDetails;
