import React, { Component } from "react";
import {
  SearchContent,
  FontColor,
  Tag,
  GlobalStyle,
  FlagWrapper,
  Divider,
  SidebarIcon
} from "./style";
import { Container, Row, Col } from "react-bootstrap";
import { connect } from "react-redux";
import * as api from "../../../api/apiList";
import API from "../../../api";
import {
  showSidebar,
  showSearchbar,
  setSearchText,
  setImportant,
  setMailType
} from "../../../store/actions/headerActions";
import { setMailList } from "../../../store/actions/mailAction";

class Searchbar extends Component {
  renderLabel = () => {
    const { headerColor, label } = this.props.currentMailType;

    return (
      <SearchContent onClick={this.props.showSearchbar}>
        Search in
        <FontColor typeColorCode={headerColor}>
          {` ${label === "all" ? "uKey mail inbox" : label}... `}
        </FontColor>
      </SearchContent>
    );
  };

  handleKey = e => {
    if (e.keyCode === 8) {
      const { searchText, isImportant, currentMailType } = this.props;
      if (searchText.length === 0 && isImportant) {
        this.props.setImportant(false);
      } else if (searchText.length === 0 && currentMailType.label !== "") {
        this.props.setMailType("");
      }
    }
  };

  renderSearchbox = () => {
    const { currentMailType, isImportant } = this.props;

    return (
      <>
        {currentMailType.label !== "" && (
          <Tag typeColorCode={currentMailType.headerColor}>
            {currentMailType.label}
          </Tag>
        )}

        {isImportant && (
          <FlagWrapper className="ml-2 ml-sm-3">
            <img src="assets/images/flagIcon_fill.png" alt="Flag Icon" />
          </FlagWrapper>
        )}
        <Divider className="d-none d-sm-inline-block">
          <img src="assets/images/line.jpg" alt="Divider" />
        </Divider>
        <input
          className="search-input mt-2 mt-md-0"
          type="text"
          name="search"
          placeholder="Search..."
          onKeyUp={this.handleKey}
          onChange={this.searchMail}
          onKeyDown={this.handleKeyDown}
        />
      </>
    );
  };

  handleKeyDown = event => {
    if (event.key === "Enter") {
      API.post(api.GET_EMAIL_LIST, {
        mailBox: "inbox",
        filter: {
          text: event.target.value
        }
      }).then(res => {
        this.props.setMailList(res.data.data);
      });
    }
  };

  searchMail = event => {
    this.props.setSearchText(event.target.value);
  };

  render() {
    const { searchClick } = this.props;
    return (
      <>
        <GlobalStyle />
        <Container fluid>
          <Row className="px-2 py-3 p-md-4">
            <Col
              lg={2}
              md={2}
              sm={2}
              xs={12}
              className="text-center text-md-left"
              onClick={this.props.showSidebar}
            >
              <SidebarIcon src="assets/images/sidebarIcon.png" alt="Sidebar" />
            </Col>
            <Col lg={10} md={10} sm={10} xs={12} className="mt-3 mt-md-0">
              <img
                src="assets/images/searchIcon.png"
                alt="Search"
                className="mr-2 mr-sm-3 search-icon"
              />

              {searchClick ? this.renderSearchbox() : this.renderLabel()}
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapState = state => {
  const {
    currentMailType,
    searchClick,
    isImportant,
    searchText
  } = state.headerReducer;
  const { mailList } = state.mailReducer;
  return {
    currentMailType,
    searchClick,
    isImportant,
    searchText,
    mailList
  };
};

const mapdispatch = dispatch => {
  return {
    showSidebar: () => {
      dispatch(showSidebar());
    },
    showSearchbar: () => {
      dispatch(showSearchbar());
    },
    setSearchText: data => {
      dispatch(setSearchText(data));
    },
    setImportant: data => {
      dispatch(setImportant(data));
    },
    setMailType: data => {
      dispatch(setMailType(data));
    },
    setMailList: data => {
      dispatch(setMailList(data));
    }
  };
};

export default connect(
  mapState,
  mapdispatch
)(Searchbar);
