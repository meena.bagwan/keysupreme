import styled, { createGlobalStyle } from 'styled-components';
import { device } from '../../../theme/device';

export const GlobalStyle = createGlobalStyle`
  .search-input{
    background-color: transparent;
    border: 0;
    font-family: 'Open Sans', sans-serif;
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 24px;
    color: #505050;
    display: inline-block;
    margin-left:16px;
    letter-spacing: 0.8px;
    width: 50%;

    &:placeholder {
      color: #969696
    }

    &:focus {
      outline: unset;
    }

    @media ${device.mobileS} {
      font-size: 14px;
      margin-left:10px;
      width: 90%;
    }

    @media ${device.tablet} {
      font-size: 16px;
      margin-left:16px;
      width: 50%;
    }

  }
`;

export const SidebarIcon = styled.img`
  display: inline-block;
  cursor: pointer;
`;

export const SearchContent = styled.div`
  font-family: "Open Sans", sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 24px;
  color: #505050;
  display: inline-block;
  cursor: pointer;
  margin-left: 20px;

  @media ${device.mobileS} {
    font-size: 14px;
    margin-left: 0;
  }

  @media ${device.laptop} {
    font-size: 16px;
  }
`;

export const FontColor = styled.span`
  color: ${(props) => props.typeColorCode};
  text-transform: capitalize;
`;

export const Tag = styled.span`
  background: ${(props) => props.typeColorCode};
  text-transform: capitalize;
  border-radius: 6px;
  color: #ffffff;
  font-family: sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 24px;
  padding: 5px 10px;
  letter-spacing: 0.8px;
  margin-left: 20px;

  @media ${device.mobileS} {
    margin-left: 0;
    font-size: 14px;
  }

  @media ${device.laptop} {
    margin-left: 20px;
  }
`;

export const FlagWrapper = styled.span`
  background: black;
  padding: 0 5px;
  border-radius: 5px;
  display: inline-block;
  text-align: center img {
    width: 75%;
  }
`;

export const Divider = styled.span`
  margin-left: 16px;
  img {
    top: -6px;
    position: absolute;
    width: 3px;
  }

  @media ${device.mobileS} {
    margin-left: 10px;
  }

  @media ${device.tablet} {
    margin-left: 16px;
  }
`;
