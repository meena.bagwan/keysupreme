import styled, { createGlobalStyle } from "styled-components";
import { device } from "../../theme/device";

export const GlobalStyle = createGlobalStyle`
.react-datepicker-popper{
    margin-top: 15px !important;
    margin-left: -40px !important;
    left: -60px !important;
}
.react-datepicker__input-container{
  display:none !important
}

input[type=checkbox]{
	height: 0;
	width: 0;
  visibility: hidden;
  margin: 0;
}

label {
	cursor: pointer;
	text-indent: -9999px;
	width: 2.5em;
	height: 1em;
	background: rgb(0, 0, 0, 0.2);
	border-radius: 100px;
  position: relative;
  vertical-align: middle;
  margin: 0;
}

label:after {
	content: '';
	position: absolute;
	top: -2px;
	left: 0;
	width: 20px;
	height: 20px;
	background: #d0d0d0;
	border-radius: 90px;
	transition: 0.2s;
	box-shadow: 0 0.1em 0.3em rgba(0,0,0,0.3);
}

input:checked + label {
	background: rgba(255, 255, 255, 0.5)
}

input:checked + label:after {
	left: calc(100% - 0px);
	transform: translateX(-100%);
	background: #ffffff;
}

.react-datepicker__day {
  border-radius: 50% !important;
  
  &:hover {
    border-radius: 50% !important;
  }
}

.react-datepicker__day--in-range, .react-datepicker__day--in-selecting-range {
  color: #000 !important;
  background-color: #e9e9e9 !important; 
}

.react-datepicker__day--range-start, .react-datepicker__day--selecting-range-start, .react-datepicker__day--range-end, .react-datepicker__day--selecting-range-end, .react-datepicker__day--selected,.react-datepicker__day--keyboard-selected {
  background-color: #212121 !important;
  color: white !important;
}

.popover {
  top: 9px !important;
  border: unset
}

.popover-body {
  box-shadow: 0 0 0 0.2rem rgba(0,123,255,.25)
}

.popover-body .form-control {
  border: unset !important;
  border-bottom: 1px solid #ced4da !important; 
  border-radius: unset;
  padding: 30px 10px
  &:focus {
    box-shadow: unset;
  }
}

.react-datepicker__header {
  background-color: unset !important;
  border-bottom: unset !important;
}
 .no-padding {
    padding: 10px 0 !important;
  }
`;

export const MenuWrapper = styled.div`
  background: ${props => props.typeColorCode};
  margin: 0;
  padding: 0;

  ul {
    margin: 0;
    padding: 0;
  }

  .label-container {
    white-space: nowrap;
    overflow: hidden;
  }

  @media ${device.mobileS} {
    padding: 10px 0;
  }

  @media ${device.tablet} {
    padding: 0;
  }
  .corners-fade {
    position: relative;

    @media ${device.tablet} {
      &:before {
        ${({ showLeftArrow }) => showLeftArrow && ` content: "";`}
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        width: 5em;
        // background-image: linear-gradient(to right, transparent 0%, white 100%);
        background-image: linear-gradient(
          to left,
          transparent 0,
          ${props => (props.isGradient ? "#4ab2ad" : props.typeColorCode)} 100%
        );
      }

      &:after {
        ${({ showRightArrow }) => showRightArrow && ` content: "";`}
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        width: 5em;
        // background-image: linear-gradient(to left, transparent 0%, white 100%);

        background-image: linear-gradient(
          to right,
          transparent 0,
          ${props => (props.isGradient ? "#3953bc" : props.typeColorCode)} 100%
        );
      }
    }
  }
`;

export const MenuItem = styled.li`
  color: #ffffff;
  display: inline-block;
  padding: 10px 40px;
  font-family: sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 24px;
  letter-spacing: 0.8px;
  cursor: pointer;

  &.active {
    background: rgba(255, 255, 255, 0.2);
    border-radius: 10px;
    padding: 3px 40px;

    @media ${device.mobileS} {
      padding: 10px 40px;
    }

    @media ${device.tablet} {
      padding: 3px 20px;
    }

    @media ${device.laptop} {
      padding: 3px 40px;
    }
  }

  @media ${device.mobileS} {
    width: 100%;
    text-align: center;
    padding: 10px 40px;
  }

  @media ${device.tablet} {
    width: unset;
    text-align: unset;
    padding: 10px 20px;
  }
  @media ${device.laptop} {
    padding: 10px 40px;
  }
`;

export const FeaturedHeader = styled.div`
  background: #262626;
  color: white;
  padding: 10px;
  cursor: pointer;
  a {
    color: white;
    text-decoration: none;
  }
`;

export const BackArrowStyle = styled.span`
  margin-right: 30px;
`;
export const Text = styled.p`
  display: inline-block;
`;
export const OkButton = styled.div`
  text-align: center;
`;

export const ColorSample = styled.div`
  background-color: ${props => props.color};
  width: 50px;
  height: 30px;
  margin-left: 10px;
  border: 2px solid #504a4a;
  border-radius: 2px;
`;

export const RightArrow = styled.span`
  text-align: center;
  color: white;
  position: absolute;
  top: 0;
  right: 20px;
  font-weight: bolder;
  display: flex;
  align-items: center;
  height: 100%;
  cursor: pointer;
  z-index: 99;
`;

export const LeftArrow = styled.span`
  text-align: center;
  color: white;
  top: 0;
  left: 20px;
  position: absolute;
  font-weight: bolder;
  display: flex;
  align-items: center;
  height: 100%;
  cursor: pointer;
`;
