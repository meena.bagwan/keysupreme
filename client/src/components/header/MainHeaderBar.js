import React, { Component } from "react";
import {
  MenuItem,
  MenuWrapper,
  GlobalStyle,
  ColorSample,
  OkButton,
  RightArrow,
  LeftArrow
} from "./style";
import {
  Container,
  Form,
  OverlayTrigger,
  Popover,
  Row,
  Col
} from "react-bootstrap";
// import { Collapse, Button, CardBody, Card } from 'reactstrap';
import { connect } from "react-redux";
import {
  setMailType,
  showSearchbar,
  setImportant,
  getLabels,
  addLabel
} from "../../store/actions/headerActions";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { menubarLabels } from "../../labelsConstant/enLanguageLabels";
import API from "../../api";
import * as api from "../../api/apiList";
import { getMailList } from "../../store/actions/mailAction";
import moment from "moment";
import { SketchPicker } from "react-color";
import { capitalize, toLower } from "lodash";

class MainHeaderBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // startDate: new Date()
      startDate: "",
      endDate: "",
      selectionComplete: false,
      collapse: false,
      status: "Closed",
      showColorPicker: false,
      newLabelColor: "#e8e8e8",
      newLabelText: "",
      showLeftArrow: false,
      showRightArrow: false
    };
  }

  componentDidMount() {
    this.props.getLabels();
    this.manageScroll();
  }

  toggleColorPicker = () => {
    this.setState(prev => {
      return { showColorPicker: !prev.showColorPicker };
    });
  };

  handleLabelPopover = () => {
    const { newLabelColor, newLabelText } = this.state;
    if (newLabelText !== "") {
      let newLabelData = {
        text: capitalize(newLabelText),
        color: newLabelColor,
        value: toLower(newLabelText)
      };
      this.props.addLabel(newLabelData);
    }

    this.refs.labelOverlay.hide();
    this.setState(
      {
        newLabelColor: "#e8e8e8",
        newLabelText: "",
        showColorPicker: false
      },
      () => {
        this.refs.menubar.scrollLeft =
          this.refs.menubar.scrollWidth - this.refs.menubar.clientWidth;
      }
    );
  };

  openDatepicker = () => {
    this._calendar.setOpen(true);
  };

  setFlag = event => {
    this.props.setImportant(event.target.checked);
  };

  onDragOver = e => {
    e.preventDefault();
  };

  onDrop = (e, label) => {
    let mailId = e.dataTransfer.getData("id");

    API.post(api.SET_FLAG, {
      flags: {
        key: "update",
        value: true
      },
      uid: mailId
    }).then(res => {
      this.props.getMailList();
    });
  };

  handleDateChange = date => {
    const { startDate, endDate, selectionComplete } = this.state;

    if (!selectionComplete && !startDate) {
      this.setState({
        startDate: date
      });
      return;
    }

    if (
      !selectionComplete &&
      startDate &&
      !endDate &&
      moment(date).isBefore(moment(startDate), "day")
    ) {
      this.setState({
        startDate: date,
        endDate: "",
        selectionComplete: false
      });
      return;
    }

    if (!selectionComplete && startDate && !endDate) {
      this.setState({
        endDate: date,
        selectionComplete: true
      });
      return;
    }

    if (selectionComplete && startDate && endDate) {
      this.setState({
        startDate: date,
        endDate: "",
        selectionComplete: false
      });
      return;
    }
  };
  formatDate(string) {
    var options = { year: "numeric", month: "long", day: "numeric" };
    return new Date(string).toLocaleDateString([], options);
  }
  handleSelect = date => {
    const { startDate, endDate, selectionComplete } = this.state;
    if (!selectionComplete && startDate && !endDate) {
      this.handleDateChange(date);
    }
  };

  selectionDone = () => {
    this._calendar.setOpen(false);
  };

  handleColorChange = color => {
    this.setState({ newLabelColor: color.hex });
  };

  handleLabelText = e => {
    this.setState({
      newLabelText: e.target.value
    });
  };
  scrollMenuLeft = () => {
    let scrollValue =
      this.refs.menubar.clientWidth < 420
        ? this.refs.menubar.clientWidth
        : this.refs.menubar.clientWidth / 4;
    // this.refs.menubar.scrollLeft -= 40;
    this.refs.menubar.scrollLeft -= scrollValue;
  };

  scrollMenuRight = () => {
    let scrollValue =
      this.refs.menubar.clientWidth < 420
        ? this.refs.menubar.clientWidth
        : this.refs.menubar.clientWidth / 4;
    this.refs.menubar.scrollLeft += scrollValue;
    console.log(this.refs.menubar.clientWidth);
  };

  manageScroll = () => {
    if (
      this.refs.menubar.scrollLeft !== 0 &&
      this.refs.menubar.scrollLeft !==
        this.refs.menubar.scrollWidth - this.refs.menubar.clientWidth
    ) {
      this.setState({
        showLeftArrow: true,
        showRightArrow: true
      });
    } else if (this.refs.menubar.scrollLeft === 0) {
      this.setState({
        showRightArrow: true,
        showLeftArrow: false
      });
    } else {
      this.setState({
        showLeftArrow: true,
        showRightArrow: false
      });
    }
  };

  render() {
    const { label, headerColor } = this.props.currentMailType;

    const MyContainer = ({ className, children }) => {
      return (
        <div className={className}>
          <div style={{ position: "relative" }}>{children}</div>
          <OkButton onClick={this.selectionDone}>Ok</OkButton>
        </div>
      );
    };

    return (
      <>
        <GlobalStyle />

        <MenuWrapper
          typeColorCode={headerColor}
          isGradient={label === "all" ? true : false}
          showLeftArrow={this.state.showLeftArrow}
          showRightArrow={this.state.showRightArrow}
        >
          <Container>
            <Row>
              <Col md={8} className="corners-fade">
                {this.state.showLeftArrow && (
                  <LeftArrow onClick={this.scrollMenuLeft}>{"<"}</LeftArrow>
                )}
                <ul
                  className="label-container"
                  ref="menubar"
                  onScroll={this.manageScroll}
                >
                  {this.props.labels.map((eachLabel, index) => {
                    return (
                      <MenuItem
                        key={index}
                        className={
                          eachLabel.value === label ||
                          (index === 0 && label === "")
                            ? "active"
                            : ""
                        }
                        onClick={() =>
                          this.props.setMailType(eachLabel) ||
                          this.props.history.push("/")
                        }
                      >
                        {eachLabel.text}
                      </MenuItem>
                    );
                  })}
                </ul>

                {this.state.showRightArrow && (
                  <RightArrow onClick={this.scrollMenuRight}>{">"}</RightArrow>
                )}
              </Col>
              <Col md={4}>
                <ul>
                  <MenuItem>
                    {/* <img
                src="assets/images/addNewLabel.png"
                alt="Add Icon"
                onClick={this.toggle}
              /> */}

                    <OverlayTrigger
                      trigger="click"
                      rootClose
                      ref="labelOverlay"
                      key="bottom"
                      placement="bottom"
                      overlay={
                        <Popover id="popover-basic">
                          <Form>
                            <Form.Group
                              as={Row}
                              controlId="formHorizontalEmail"
                            >
                              <Col sm={12}>
                                <Form.Control
                                  type="text"
                                  placeholder="Add new label name..."
                                  onChange={this.handleLabelText}
                                />
                              </Col>
                              <Col sm={12}>
                                <div
                                  onClick={this.toggleColorPicker}
                                  className="d-flex p-2"
                                >
                                  <img src="assets/images/fill.png" />
                                  <ColorSample
                                    color={this.state.newLabelColor}
                                  />
                                </div>
                                {this.state.showColorPicker && (
                                  <>
                                    <div
                                      className="text-right"
                                      onClick={this.toggleColorPicker}
                                    >
                                      x
                                    </div>
                                    <SketchPicker
                                      color={this.state.newLabelColor}
                                      onChangeComplete={this.handleColorChange}
                                      width="92%"
                                    />
                                  </>
                                )}
                              </Col>
                              <Col sm={12}>
                                <div
                                  className="text-center"
                                  onClick={this.handleLabelPopover}
                                >
                                  Ok
                                </div>
                              </Col>
                            </Form.Group>
                          </Form>
                        </Popover>
                      }
                    >
                      <img src="assets/images/addNewLabel.png" alt="Add Icon" />
                    </OverlayTrigger>
                  </MenuItem>

                  <MenuItem className="float-none float-md-right no-padding">
                    <span className="mr-5">
                      <DatePicker
                        selected={this.state.startDate}
                        onChange={this.handleDateChange}
                        onSelect={this.handleSelect}
                        selectsEnd={Boolean(this.state.startDate)}
                        startDate={this.state.startDate}
                        endDate={this.state.endDate}
                        ref={c => (this._calendar = c)}
                        shouldCloseOnSelect={false}
                        calendarContainer={MyContainer}
                      />
                      <img
                        src="assets/images/calenderIcon.png"
                        alt="Calender Icon"
                        onClick={this.openDatepicker}
                      />
                    </span>
                    <span className="mr-3">
                      <img
                        src={
                          this.props.isImportant
                            ? "assets/images/flagIcon_fill.png"
                            : "assets/images/flagIcon.png"
                        }
                        alt="Flag Icon"
                      />
                    </span>
                    <span>
                      <input
                        type="checkbox"
                        id="switch"
                        onClick={this.setFlag}
                      />
                      <label htmlFor="switch">Toggle</label>
                    </span>
                  </MenuItem>
                </ul>
              </Col>
            </Row>

            {/*             
<MenuItem
              className={label === "all" || label === "" ? "active" : ""}
              onClick={() =>
                this.props.setMailType("all") || this.props.history.push("/")
              }
            >
              {menubarLabels.ALLINBOX}
            </MenuItem>
            <MenuItem
              className={label === "update" ? "active droppable" : "droppable"}
              onClick={() => this.props.setMailType("update")}
              onDragOver={e => this.onDragOver(e)}
              onDrop={e =>
                this.onDrop(e, "update") || this.props.history.push("/")
              }
            >
              {menubarLabels.UPDATE}
            </MenuItem>

            <MenuItem
              className={label === "personal" ? "active" : ""}
              onClick={() =>
                this.props.setMailType("personal") ||
                this.props.history.push("/")
              }
            >
              {menubarLabels.PERSONAL}
            </MenuItem>

            <MenuItem
              className={label === "work" ? "active" : ""}
              onClick={() =>
                this.props.setMailType("work") || this.props.history.push("/")
              }
            >
              {menubarLabels.WORK}
            </MenuItem> */}
          </Container>
        </MenuWrapper>
      </>
    );
  }
}

const mapState = state => {
  const { currentMailType, isImportant, labels } = state.headerReducer;
  return {
    currentMailType,
    isImportant,
    labels
  };
};

const mapdispatch = dispatch => {
  return {
    setMailType: data => {
      dispatch(setMailType(data));
    },
    setSearchbar: () => {
      dispatch(showSearchbar());
    },
    setImportant: data => {
      dispatch(setImportant(data));
    },
    getMailList: () => {
      dispatch(getMailList());
    },
    getLabels: () => {
      dispatch(getLabels());
    },
    addLabel: data => {
      dispatch(addLabel(data));
    }
  };
};

export default connect(
  mapState,
  mapdispatch
)(MainHeaderBar);
