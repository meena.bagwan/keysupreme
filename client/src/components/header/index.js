import React, { Component } from "react";
import { GlobalStyle, FeaturedHeader, BackArrowStyle } from "./style";
import MainHeaderBar from "./MainHeaderBar";
import { Container, Row, Col } from "react-bootstrap";
import "react-datepicker/dist/react-datepicker.css";
import { connect } from "react-redux";
import { setFullScreenMode,setMinimiseScreenMode } from "../../store/actions/mailAction";

class Header extends Component {
  backMainPage = () => {
    this.props.history.goBack();
  };

  exitFullScreenMode = () => {
    this.props.setFullScreenMode(false);
    this.props.setMinimiseScreenMode(true);
    this.props.history.push("/");
  };

  render() {
    const { pathname } = this.props.location;

    return (
      <>
        <GlobalStyle />
        {pathname === "/" && <MainHeaderBar {...this.props}/>}
        {pathname === "/sentEmailList"  && <MainHeaderBar {...this.props} />}
        {pathname === "/deleteEmailList"  && <MainHeaderBar {...this.props} />}
        {pathname === "/draftEmailList"  && <MainHeaderBar {...this.props} />}
        {pathname === "/email-details" && (
          <FeaturedHeader onClick={this.backMainPage}>
            <Container>
              <BackArrowStyle>
                <img src="assets/images/backArrow.png" alt="back arrow" />
              </BackArrowStyle>
              {this.props.location.state.email.subject}
            </Container>
          </FeaturedHeader>
        )}
        {pathname === "/newEmailPage" && (
          <FeaturedHeader>
            <Container>
              <Row>
                <Col xs={12} md={8} lg={8} onClick={this.backMainPage}>
                  <BackArrowStyle onClick={this.backMainPage}>
                    <img src="assets/images/backArrow.png" alt="back arrow" />
                  </BackArrowStyle>
                  New email
                </Col>
                <Col md={4} lg={4} className="d-none d-md-block text-right">
                  <span onClick={this.exitFullScreenMode}>
                    Exit full screen writing
                  </span>
            
                  <img
                    src="/assets/images/exitFullScreen.png"
                    alt="Exit Full Screen"
                    className="pl-3"
                    onClick={this.exitFullScreenMode}
                  />
                </Col>
              </Row>
            </Container>
          </FeaturedHeader>
        )}
      </>
    );
  }
}

const mapDispatch = dispatch => {
  return {
    setFullScreenMode: data => dispatch(setFullScreenMode(data)),
    setMinimiseScreenMode : data => dispatch(setMinimiseScreenMode(data))
  };
};

export default connect(
  null,
  mapDispatch
)(Header);
