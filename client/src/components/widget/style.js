import styled, { createGlobalStyle } from "styled-components";
import { device } from "../../theme/device";

export const GlobalStyle = createGlobalStyle`
 

  .width-to-wrap-text {
    max-width: 90%

    @media ${device.mobileS} {
      max-width: 60%
    }

    @media ${device.tablet} {
      max-width: 85%
    }

    @media ${device.laptop} {
      max-width: 90%
    }
  }
`;

export const ButtonWrapper = styled.div`
  position: fixed;
  bottom: 5%;
  right: 4%;
  padding: 14px;
  border-radius: 50%;
  img {
    width: 75%;
  }
  @media ${device.mobileS} {
    right: -2%;
    // right: -5px;
  }
  @media ${device.tablet} {
    right: -2%;
    // right: -15px;
  }
  @media ${device.laptop} {
    right: 4%;
    // right: -4px;
  }
  @media ${device.laptopL} {
    right: 7%;
    // right: 108px;
  }
`;

export const ImgCursor = styled.span`
  cursor: pointer;
  img {
  width: 75%;
  display: block;
  padding-bottom: 20px;
  }
`;
