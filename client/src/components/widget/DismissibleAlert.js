import React, { useState, useEffect } from "react";
import { Alert } from "react-bootstrap";

const DismissibleAlert = ({ message, styleClass }) => {
  const [show, setShow] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setShow(false);
    }, 20000);
  }, [show]);

  return (
    show && (
      <Alert variant="dark" className={styleClass}>
        {message}
      </Alert>
    )
  );
};
export default DismissibleAlert;
