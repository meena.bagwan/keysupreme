import React, { Component } from "react";
import { Spinner } from "react-bootstrap";

class GlobalLoder extends Component {
  render() {
    return (
      <div className="overlay">
        <div className="loader">
          <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
          </Spinner>
        </div>
      </div>
    );
  }
}

export default GlobalLoder;
