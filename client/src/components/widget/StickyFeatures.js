import React, { Component } from "react";
import { ButtonWrapper, ImgCursor } from "./style";
import API from "../../api";
import * as api from "../../api/apiList";
import { connect } from "react-redux";
import {
  setMailSentFlag,
  setLoader,
  resetLoader
} from "../../store/actions/mainActions";
import {
  setComposeMailData,
  sendMail,
  draftMail,
  setMinimiseScreenMode,
  setFullScreenMode
} from "../../store/actions/mailAction";
import { Container } from "react-bootstrap";
import { checkComposeMailSchema } from "../../utils/validation";

class StickyFeatures extends Component {
  state = {
    error: {},
    notValidate:false
  };

  openNewEmailPage = () => {
    this.props.history.push("/newEmailPage");
  };

  sendMail = () => {

    let mailData = {
      to: this.props.data.to,
      cc: this.props.data.cc,
      bcc: this.props.data.bcc,
      fromAddress: this.props.loginData.email,
      fromName: this.props.loginData.userName,
      subject: this.props.data.subject,
      text: this.props.data.content,
      html: this.props.data.htmlContent
    };
    
    checkComposeMailSchema
      .validate(mailData)
      .then(res => {
        if (this.props.minimiseScreen && !this.props.fullScreen) {
          this.props.sendMail(mailData);
          this.props.setMinimiseScreenMode(false);
          this.props.setFullScreenMode(true);
        } else {
          this.props.setComposeMailData(mailData);
          this.props.history.push("/");
        }
      })
      .catch(err => {
        let errors = {
          hasError: true,
          message: err.errors[0]
        };
        this.props.setError(errors);
      });
  };

  draftMail = () => {
    let draftData =  {
      to: this.props.data.to,
      cc: this.props.data.cc,
      bcc: this.props.data.bcc,
      fromAddress: this.props.loginData.email,
      fromName: this.props.loginData.userName,
      subject: this.props.data.subject,
      text: this.props.data.content,
      html: this.props.data.htmlContent
    }
    this.props.draftMail(draftData);
  }
  render() {
    const { pathname } = this.props.location;
    return (
      <Container>
        <ButtonWrapper>
          {pathname === "/" && !this.props.minimiseScreen && (
            <ImgCursor>
              <img
                src="assets/images/composeEmailNew.png"
                alt=""
                onClick={() => this.openNewEmailPage()}
              />
            </ImgCursor>
          )}
          {pathname === "/" && this.props.minimiseScreen && (
            <ImgCursor>
              <img
                src="assets/images/send.png"
                alt=""
                onClick={this.sendMail}
              />
            </ImgCursor>
          )}
          {pathname === "/email-details" && (
            <ImgCursor>
              <img src="assets/images/groupMessage.png" alt="" />
            </ImgCursor>
          )}
          {pathname === "/newEmailPage" && (
            <div>
              <ImgCursor>
                <img
                  src="assets/images/draftButton.png"
                  alt=""
                  onClick={this.draftMail}
                />
              </ImgCursor>
              <ImgCursor>
                <img
                  src="assets/images/scheduleButton.png"
                  alt=""
                />
              </ImgCursor>
              <ImgCursor>
                <img
                  src="assets/images/send.png"
                  alt=""
                  onClick={this.sendMail}
                />
              </ImgCursor>
            </div>

          )}
        </ButtonWrapper>
      </Container>
    );
  }
}

const mapState = state => {
  const { loginData, mailSent } = state.mainReducer;
  const { minimiseScreen, fullScreen } = state.mailReducer;
  return {
    loginData,
    mailSent,
    minimiseScreen,
    fullScreen
  };
};
const mapdispatch = dispatch => {
  return {
    sentMailFlag: data => {
      dispatch(setMailSentFlag(data));
    },
    setComposeMailData: data => {
      dispatch(setComposeMailData(data));
    },
    sendMail: data => {
      dispatch(sendMail(data));
    },
    setMinimiseScreenMode: data => {
      dispatch(setMinimiseScreenMode(data));
    },
    setFullScreenMode: data => {
      dispatch(setFullScreenMode(data));
    },
    draftMail: data => {
      dispatch(draftMail(data));
    }
  };
};

export default connect(
  mapState,
  mapdispatch
)(StickyFeatures);
