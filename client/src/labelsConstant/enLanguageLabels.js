export const menubarLabels = {

  ALLINBOX: 'All inbox',
  UPDATE: 'Update',
  PERSONAL: 'Personal',
  WORK: 'Work',
};
export const sidebarLabels = {
  SENT: 'Sent',
  DRAFT: 'Draft',
  FILES: 'Files',
  SCHEDULED: 'Scheduled',
  SETTING: 'Settings',
  TRASH: 'Trash',
};
