import React from "react";
import { Route } from "react-router-dom";
import Sidebar from "../components/sidebar";
import Header from "../components/header";
import Searchbar from "../components/header/searchbar";
import { GlobalStyle } from "./style";
export const AuthRoute = ({ component: Component, title, ...rest }) => (
  <>
    <GlobalStyle />
    <Route
      {...rest}
      render={props => (
        <>
          <section>
            <Searchbar {...props} />
            <Header {...props} />
          </section>
          {/* <section className="cal-height"> */}
          <section className="full-height overflow-auto hide-scrollbar">
            {/* <section> */}
            <Sidebar {...props} />
            <Component {...props} />
          </section>
        </>
      )}
    />
  </>
);
