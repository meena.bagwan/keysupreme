import React, { Component } from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import { AuthRoute } from "./AuthRoute";
import EmailListing from "../components/email";
import EmailDetails from "../components/email/EmailDetails";
import NewEmailPage from "../components/email/NewEmailPage";
import SentEmailList from "../components/email/SentEmailList";
import DeleteEmailList from "../components/email/DeleteEmailList";
import DraftEmailList from "../components/email/DraftEmailList"

export default class MainRouter extends Component {

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <AuthRoute path="/" exact component={EmailListing} />
          <AuthRoute path="/newEmailPage" component={NewEmailPage} />
          <AuthRoute path="/email-details" component={EmailDetails} />
          <AuthRoute path="/sentEmailList" component={SentEmailList} />
          <AuthRoute path="/deleteEmailList" component={DeleteEmailList} />
          <AuthRoute path="/draftEmailList" component={DraftEmailList} />
        </Switch>
      </BrowserRouter>
    );
  }
}
