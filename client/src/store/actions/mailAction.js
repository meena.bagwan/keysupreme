import {
  SET_MAIL_LIST,
  SET_FULL_SCREEN_MODE,
  SET_COMPOSE_MAIL_DATA,
  SET_DRAFT_MAIL_DATA,
  SET_MINIMISE_SCREE_MODE,
  SET_SENT_MAIL_LIST,
  SET_DELETE_MAIL_LIST,
  SET_DRAFT_MAIL_LIST
} from "./actionConstants";
import { setLoader, resetLoader, setMailSentFlag } from "./mainActions";
import { loginData } from "./mainActions";
import API from "../../api";
import * as api from "../../api/apiList";

export function setMailList(data) {
  return {
    type: SET_MAIL_LIST,
    payload: data
  };
}
export function setSentMailList(data) {
  return {
    type: SET_SENT_MAIL_LIST,
    payload: data
  }
}

export function setDeleteMailList(data) {
  return {
    type: SET_DELETE_MAIL_LIST,
    payload: data
  }
}
export function setDraftMailList(data) {
  return {
    type: SET_DRAFT_MAIL_LIST,
    payload: data
  }
}

export function setComposeMailData(data) {
  return {
    type: SET_COMPOSE_MAIL_DATA,
    payload: data
  };
}

export function setDraftMailData(data) {
  return {
    type: SET_DRAFT_MAIL_DATA,
    payload: data
  }
}

export function setFullScreenMode(data) {
  return {
    type: SET_FULL_SCREEN_MODE,
    payload: data
  };
}

export function setMinimiseScreenMode(data) {
  return {
    type: SET_MINIMISE_SCREE_MODE,
    payload: data
  };
}

export function getSentMailList() {
  return (dispatch, getState) => {
    const { sentMailList } = getState().mailReducer;
    if (sentMailList.length === 0) dispatch(setLoader());
    API.post(api.GET_EMAIL_LIST, {
      mailBox: "inbox.Sent"
    }).then(res => {
      dispatch(setSentMailList(res.data.data));
      dispatch(resetLoader());
    });
  }
}

export function getDeleteList()
{
  return (dispatch, getState) => {
    const { deleteMailList } = getState().mailReducer;
     if (deleteMailList.length === 0) dispatch(setLoader());
      API.post(api.GET_EMAIL_LIST, {
        mailBox: "INBOX",
        filter: {
          flag:"DELETED"
        }}).then(res => {
        dispatch(setDeleteMailList(res.data.data));
        dispatch(resetLoader());
      });   
  };
}
export function getDraftList()
{
  return (dispatch, getState) => {
    const { draftMailList } = getState().mailReducer;
     if (draftMailList.length === 0) dispatch(setLoader());
      API.post(api.GET_EMAIL_LIST, {
        mailBox: "INBOX.Drafts",
        }).then(res => {
        dispatch(setDraftMailList(res.data.data));
        dispatch(resetLoader());
      });   
  };
}
export function getMailList() {
  return (dispatch, getState) => {
    const { mailList } = getState().mailReducer;
    localStorage.removeItem("token");
    API.post(api.LOGIN, {
      hash: "meena.bagwan@email.test.keysupre.me",
      service: "test"
    }).then(({ data }) => {
      if (mailList.length === 0) dispatch(setLoader());
      localStorage.setItem("token", data.data.token);
      dispatch(loginData(data.data));
      API.post(api.GET_EMAIL_LIST, {
        mailBox: "inbox"
      }).then(res => {
        dispatch(setMailList(res.data.data));
        dispatch(resetLoader());
      });
    });
  };
}

export function sendMail(data) {
  return dispatch => {
    API.post(api.SEND_MAIL, data).then(response => {
      if (!response.data.error) {
        dispatch(setMailSentFlag(true));
        dispatch(setComposeMailData({}));
        dispatch(getMailList({}));   
      }
    });
  };
}

export function draftMail(data) {
  return dispatch => {
    API.post(api.DRAFT_MAIL,data).then(response => {
      if (!response.data.error) {
        dispatch(setDraftMailData({}));
      }
    })
  }
}
