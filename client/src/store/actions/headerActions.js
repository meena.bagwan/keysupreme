import {
  SHOW_SIDEBAR,
  HIDE_SIDEBAR,
  SET_MAIL_TYPE,
  SHOW_CALENDAR,
  HIDE_CALENDAR,
  SHOW_SEARCHBAR,
  HIDE_SEARCHBAR,
  SET_SEARCH_TEXT,
  SET_IMPORTANT,
  GET_LABELS,
  SET_LABELS,
  ADD_LABEL
} from "./actionConstants";
import axios from "axios";
import dummyLabels from "../../data/labels.json";

export function showSidebar() {
  return {
    type: SHOW_SIDEBAR
  };
}

export function hideSidebar() {
  return {
    type: HIDE_SIDEBAR
  };
}

export function setMailType(data) {
  return {
    type: SET_MAIL_TYPE,
    payload: data
  };
}

export function showCalendar() {
  return {
    type: SHOW_CALENDAR
  };
}

export function hideCalendar() {
  return {
    type: HIDE_CALENDAR
  };
}

export function showSearchbar() {
  return {
    type: SHOW_SEARCHBAR
  };
}

export function hideSearchbar() {
  return {
    type: HIDE_SEARCHBAR
  };
}

export function setSearchText(data) {
  return {
    type: SET_SEARCH_TEXT,
    payload: data
  };
}

export function setImportant(data) {
  return {
    type: SET_IMPORTANT,
    payload: data
  };
}

export function setLabels(data) {
  return {
    type: SET_LABELS,
    payload: data
  };
}

export function addLabel(data) {
  return {
    type: ADD_LABEL,
    payload: data
  };
}

export function getLabels() {
  return dispatch => {
    // axios.get("http://demo5861658.mockable.io/labels").then(res => {
    //   dispatch(setLabels(res.data.data));
    // });
    dispatch(setLabels(dummyLabels.data));
  };
}
