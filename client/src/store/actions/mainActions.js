import {
  LOGIN_DATA,
  SET_LOADER,
  RESET_LOADER,
  SET_MAIL_LIST,
  SET_MAIL_SENT_FLAG,
  SET_DELETE_MAIL_LIST
} from "./actionConstants";

export function loginData(data) {
  return {
    type: LOGIN_DATA,
    payload: data
  };
}

export function setLoader() {
  return {
    type: SET_LOADER
  };
}

export function resetLoader() {
  return {
    type: RESET_LOADER
  };
}

export function setMailList(data) {
  return {
    type: SET_MAIL_LIST,
    payload: data
  };
}
export function setDeleteMailList(data) {
  return {
    type: SET_DELETE_MAIL_LIST,
    payload: data
  };
}

export function setMailSentFlag(data) {
  return {
    type: SET_MAIL_SENT_FLAG,
    payload: data
  };
}
