import {
  SET_MAIL_LIST,
  SET_FULL_SCREEN_MODE,
  SET_COMPOSE_MAIL_DATA,
  SET_DRAFT_MAIL_DATA,
  SET_MINIMISE_SCREE_MODE,
  SET_SENT_MAIL_LIST,
  SET_DELETE_MAIL_LIST,
  SET_DRAFT_MAIL_LIST
} from "../actions/actionConstants";

const initialState = {
  mailList: [],
  fullScreen: true,
  minimiseScreen : false,
  sentMailList: [],
  composeMailData:{},
  draftMailData:{},
  deleteMailList:[],
  draftMailList:[]
};

const mailReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_MAIL_LIST: {
      return {
        ...state,
        mailList: [...action.payload]
      };
    }
    case SET_SENT_MAIL_LIST: {
      return {
        ...state,
        sentMailList: [...action.payload]
      }
    }
    case SET_DRAFT_MAIL_LIST: {
      return {
        ...state,
        draftMailList: [...action.payload]
      }
    }
    case SET_DELETE_MAIL_LIST: {
      return {
        ...state,
        deleteMailList: [...action.payload]
      }
    }
    case SET_FULL_SCREEN_MODE: {
      return {
        ...state,
        fullScreen: action.payload
      };
    }
    case SET_MINIMISE_SCREE_MODE: {
      return {
        ...state,
        minimiseScreen: action.payload
      }
    }
    case SET_COMPOSE_MAIL_DATA: {
      return {
        ...state,
        composeMailData: action.payload
      };
    }
    case SET_DRAFT_MAIL_DATA: {
      return {
        ...state,
        draftMailData:action.payload
      }
    }
    default:
      return state;
  }
};

export default mailReducer;
