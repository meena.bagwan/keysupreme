import {
  SHOW_SIDEBAR,
  HIDE_SIDEBAR,
  SET_MAIL_TYPE,
  SHOW_SEARCHBAR,
  HIDE_SEARCHBAR,
  SET_SEARCH_TEXT,
  SET_IMPORTANT,
  SET_LABELS,
  ADD_LABEL
} from "../actions/actionConstants";

const initialState = {
  currentMailType: {
    label: "all",
    headerColor:
      "linear-gradient(60.88deg, #4DB6AC 6.73%, #1565C0 46.85%, #673AB7 86.6%)"
  },
  searchClick: false,
  openSidebar: false,
  searchText: "",
  isImportant: false,
  labels: []
};

// This is just for now
// will change it if it'll come from adminside in API call

const headerColorByType = {
  all:
    "linear-gradient(60.88deg, #4DB6AC 6.73%, #1565C0 46.85%, #673AB7 86.6%)",
  update: "#26A69A",
  work: "#673AB7",
  personal: "#1565C0",
  blank:
    "linear-gradient(60.88deg, #4DB6AC 6.73%, #1565C0 46.85%, #673AB7 86.6%)"
};

const headerReducer = (state = initialState, action) => {
  switch (action.type) {
    // case SET_MAIL_TYPE: {
    //   return {
    //     ...state,
    //     currentMailType: {
    //       ...state.currentMailType,
    //       label: action.payload,
    //       headerColor:
    //         action.payload !== ""
    //           ? headerColorByType[action.payload]
    //           : headerColorByType["blank"]
    //     },
    //     searchClick: false,
    //     searchText: ""
    //   };
    // }
    case SET_MAIL_TYPE: {
      return {
        ...state,
        currentMailType: {
          ...state.currentMailType,
          label: action.payload.value,
          headerColor: action.payload.color
        },
        searchClick: false,
        searchText: ""
      };
    }
    case SHOW_SEARCHBAR: {
      return {
        ...state,
        searchClick: true
      };
    }
    case HIDE_SEARCHBAR: {
      return {
        ...state,
        searchClick: false
      };
    }
    case SHOW_SIDEBAR: {
      return {
        ...state,
        openSidebar: true
      };
    }
    case HIDE_SIDEBAR: {
      return {
        ...state,
        openSidebar: false
      };
    }
    case SET_SEARCH_TEXT: {
      return {
        ...state,
        searchText: action.payload
      };
    }
    case SET_IMPORTANT: {
      return {
        ...state,
        isImportant: action.payload
      };
    }
    case SET_LABELS: {
      return {
        ...state,
        labels: [...action.payload]
      };
    }
    case ADD_LABEL: {
      return {
        ...state,
        labels: [...state.labels, action.payload]
      };
    }
    default:
      return state;
  }
};

export default headerReducer;
