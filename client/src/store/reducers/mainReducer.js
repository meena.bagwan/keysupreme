import {
  LOGIN_DATA,
  SET_LOADER,
  RESET_LOADER,
  SET_MAIL_LIST,
  SET_MAIL_SENT_FLAG
} from '../actions/actionConstants';

const initialState = {
  loginData: {},
  loading: false,
  mailList:[],
  mailSent:false
};

const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_DATA: {
      return {
        ...state,
        loginData: action.payload
      };
    }

    case SET_LOADER: {
      return {
        ...state,
        loading: true
      };
    }

    case RESET_LOADER: {
      return {
        ...state,
        loading: false
      };
    }

    case SET_MAIL_LIST: {
      return {
      ...state,
      mailList: action.payload
      };
      }

    case SET_MAIL_SENT_FLAG : {
      return {
        ...state,
        mailSent:action.payload
      }
    }  
    default:
      return state;
  }
};

export default mainReducer;
