import { combineReducers } from "redux";
import headerReducer from "./headerReducer";
import mainReducer from "./mainReducer";
import mailReducer from "./mailReducer";

const rootReducers = combineReducers({
  headerReducer,
  mainReducer,
  mailReducer
});

export default rootReducers;
