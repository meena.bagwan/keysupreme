export const PROOF_READING = "employees";
export const LOGIN = "account/login";
export const GET_EMAIL_LIST = "mail/getMails";
export const SET_FLAG = "mail/setFlag";
export const SEND_MAIL = "mail/sendMail";
export const DRAFT_MAIL = "mail/draftMail";