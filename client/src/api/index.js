import axios from "axios";

const API = axios.create({
  baseURL: "http://162.243.165.4:3000/v1/ukey-email",
  headers: {
    "Content-Type": "application/json",
    "access-control-allow-origin": "*"
  }
});

API.interceptors.request.use(
  config => {
    if (localStorage.getItem("token") && !config.headers.authorization) {
      config.headers.authorization = localStorage.getItem("token");
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

export default API;
